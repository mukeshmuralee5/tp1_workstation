# TP1 Windows Client 

## Host OS :

* C:\Users\Muralee>hostname
```
LAPTOP-PQ1V32RL
```
---------------

* C:\Users\Muralee>systeminfo
```
Nom du système d’exploitation:              Microsoft Windows 10 Famille
Version du système:                         10.0.18363 N/A version 18363
```
------------
* C:\Users\Muralee>systeminfo
 ```
 [...]
Mémoire physique totale:                    15 790 Mo
[...]
 ```
---------------
* C:\Users\Muralee>wmic MemoryChip
```
[...]
Manufacturer                          Mémoire physique                                       
Micron Technology                     4ATF1G64HZ-3G2E1 
Micron Technology                     4ATF1G64HZ-3G2E1 
[...]
```

------------

## Devices : 

* C:\Users\Muralee>wmic cpu get caption, deviceid, name, status
```
Caption                              DeviceID  Name                                    Status
AMD64 Family 23 Model 96 Stepping 1  CPU0      AMD Ryzen 7 4800H with Radeon Graphics  OK
```
---------------------

* C:\Users\Muralee>wmic cpu getNumberOfCores, NumberOfLogicalProcessors/Format:List

```
NumberOfCores=8
NumberOfLogicalProcessors=16
```
--------------
* C:\Users\Muralee>wmic path win32_VideoController get name
```
Name
AMD Radeon(TM) Graphics
NVIDIA GeForce RTX 2060
```
---------------------

* C:\Users\Muralee>diskpart
```
il faut ensuite donner l'autorisation pour pouvoir continuer et avoir les resultats souhaités.
```
--------------------

* DISKPART> list disk
```
  N° disque  Statut         Taille   Libre    Dyn  GPT
  ---------  -------------  -------  -------  ---  ---
  Disque 0    En ligne        931 G octets      0 octets        *
  Disque 1    En ligne        238 G octets      0 octets        *
```
---------------
* DISKPART> select disk 0
 (il s'agit d'ici d'un disque dur hdd)
```
Le disque 0 est maintenant le disque sélectionné.
```

* DISKPART> detail disk
```
ST1000LM035-1RK172
ID du disque : {47BB255B-6760-4CA5-B685-C4F2CD51495B}
Type : SATA
État : En ligne


  N° volume   Ltr  Nom          Fs     Type        Taille   Statut     Info
  ----------  ---  -----------  -----  ----------  -------  ---------  --------
  Volume 0     D   DATA         NTFS   Partition    931 G   Sain
```
-----------------

* DISKPART> select disk 1
 (nous avons ici un disque dur de type ssd)
```
Le disque 1 est maintenant le disque sélectionné.
```


* DISKPART> detail disk
```
WDC PC SN530 SDBPNPZ-256G-1002
ID du disque : {935DDAB8-760F-46DE-82EE-319B6359C0D5}
Type : NVMe
État : En ligne


  N° volume   Ltr  Nom          Fs     Type        Taille   Statut     Info
  ----------  ---  -----------  -----  ----------  -------  ---------  --------
  Volume 1     C   OS           NTFS   Partition    219 G   Sain       Démarrag
  Volume 2         RESTORE      NTFS   Partition     17 G   Sain
  Volume 3         SYSTEM       FAT32  Partition    260 M   Sain       Système

```
----------

* C:\Users\Muralee>diskpart
(encore une fois nous devons donner l'autorisation pour pouvoir continuer)



* DISKPART> select disk 0
```
Le disque 0 est maintenant le disque sélectionné.
```
------------
* C:\Users\Muralee>diskpart

* DISKPART> list partition
```
  N° partition   Type              Taille   Décalage
  -------------  ----------------  -------  --------
  Partition 1    Principale         931 G   1024 K
```

* DISKPART> select disk 1
```
Le disque 1 est maintenant le disque sélectionné.
```

* DISKPART> list partition
```
  N° partition   Type              Taille   Décalage
  -------------  ----------------  -------  --------
  Partition 1    Système            260 M   1024 K
  Partition 2    Réservé             16 M    261 M
  Partition 3    Principale         219 G    277 M
  Partition 4    Récupération      1300 M    219 G
  Partition 5    Principale          17 G    220 G
```
------------

* DISKPART> select partition 1
```
La partition 1 est maintenant la partition sélectionnée.
```

* DISKPART> detail partition
```
Partition 1
Type    : c12a7328-f81f-11d2-ba4b-00a0c93ec93b
Masqué  : Oui
Requis  : Non
Attrib  : 0X8000000000000000
Décalage en octets : 1048576

  N° volume   Ltr  Nom          Fs     Type        Taille   Statut     Info
  ----------  ---  -----------  -----  ----------  -------  ---------  --------
* Volume 3         SYSTEM       FAT32  Partition    260 M   Sain       Système
```
-----------
* DISKPART> select partition 2
```
La partition 2 est maintenant la partition sélectionnée.
```

* DISKPART> detail partition
```
Partition 2
Type    : e3c9e316-0b5c-4db8-817d-f92df00215ae
Masqué  : Oui
Requis  : Non
Attrib  : 0X8000000000000000
Décalage en octets : 273678336
```
-------------
* DISKPART> select partition 3
```
La partition 3 est maintenant la partition sélectionnée.
```
* DISKPART> detail partition
```
Partition 3
Type    : ebd0a0a2-b9e5-4433-87c0-68b6b72699c7
Masqué  : Non
Requis  : Non
Attrib  : 0000000000000000
Décalage en octets : 290455552

  N° volume   Ltr  Nom          Fs     Type        Taille   Statut     Info
  ----------  ---  -----------  -----  ----------  -------  ---------  --------
* Volume 1     C   OS           NTFS   Partition    219 G   Sain       Démarrag
```
--------------
* DISKPART> select partition 4
```
La partition 4 est maintenant la partition sélectionnée.
```
* DISKPART> detail partition
```
Partition 4
Type    : de94bba4-06d1-4d40-a16a-bfd50179d6ac
Masqué  : Non
Requis  : Oui
Attrib  : 0X8000000000000001
Décalage en octets : 235906531328

  N° volume   Ltr  Nom          Fs     Type        Taille   Statut     Info
  ----------  ---  -----------  -----  ----------  -------  ---------  --------
* Volume 4         RECOVERY     NTFS   Partition   1300 M   Sain       Masqué
```
---------------

* Explication des partitions 

En informatique, le partitionnement d'un support de stockage (disque dur, SSD, carte-mémoire...) est l'opération qui consiste à le diviser en partitions ou régions dans lesquelles les systèmes d'exploitation présents sur la machine peuvent gérer leurs informations de manière séparée et privée.

### Users

* C:\Users\Muralee>net user
```
comptes d’utilisateurs de \\LAPTOP-PQ1V32RL


Administrateur           DefaultAccount           Invité
Muralee                  WDAGUtilityAccount
La commande s’est terminée correctement.

```
-----------
* C:\Users\Muralee>WMIC useraccount get name,sid
 ```
Name                SID
Administrateur      S-1-5-21-612620529-4104154380-200844439-500
DefaultAccount      S-1-5-21-612620529-4104154380-200844439-503
Invité              S-1-5-21-612620529-4104154380-200844439-501
Muralee             S-1-5-21-612620529-4104154380-200844439-1001
WDAGUtilityAccount  S-1-5-21-612620529-4104154380-200844439-504

```
---------------
* C:\Users\Muralee>wmic useraccount list full

```
AccountType=512
Description=Compte d'utilisateur d'administration
Disabled=TRUE
Domain=LAPTOP-PQ1V32RL
FullName=
InstallDate=
LocalAccount=TRUE
Lockout=FALSE
Name=Administrateur
PasswordChangeable=TRUE
PasswordExpires=FALSE
PasswordRequired=TRUE
SID=S-1-5-21-612620529-4104154380-200844439-500
SIDType=1
Status=Degraded

[...]

```

La ligne " Description=Compte d'utilisateur d'administration " 
Nous permeon savoir que ce compte etait en admin.
le SID m'a permis de retrouver le nom du compte via la premiere commande.

---------------
#### Processus

C:\Users\Muralee>tasklist
---
```

Nom de l’image                 PID Nom de la sessio Numéro de s Utilisation
========================= ======== ================ =========== ============
System Idle Process              0 Services                   0         8 Ko
System                           4 Services                   0     5 416 Ko
Registry                       168 Services                   0    81 580 Ko
smss.exe                       528 Services                   0     1 096 Ko
csrss.exe                      980 Services                   0     6 204 Ko
wininit.exe                    896 Services                   0     7 280 Ko
services.exe                  1052 Services                   0    14 308 Ko
lsass.exe                     1084 Services                   0    25 240 Ko
svchost.exe                   1224 Services                   0     3 652 Ko
fontdrvhost.exe               1256 Services                   0     3 284 Ko
svchost.exe                   1264 Services                   0    35 628 Ko
svchost.exe                   1360 Services                   0    18 372 Ko
svchost.exe                   1416 Services                   0     8 824 Ko
svchost.exe                   1576 Services                   0    11 844 Ko
svchost.exe                   1584 Services                   0    10 916 Ko
svchost.exe                   1592 Services                   0     8 024 Ko
svchost.exe                   1704 Services                   0     5 752 Ko
svchost.exe                   1732 Services                   0    10 616 Ko
svchost.exe                   1740 Services                   0    11 980 Ko
svchost.exe                   1920 Services                   0     8 584 Ko
svchost.exe                   2016 Services                   0    21 152 Ko
svchost.exe                   2036 Services                   0     8 400 Ko
svchost.exe                   2044 Services                   0    10 768 Ko
svchost.exe                   2080 Services                   0     6 672 Ko
svchost.exe                   2128 Services                   0    11 920 Ko
svchost.exe                   2148 Services                   0     7 712 Ko
svchost.exe                   2196 Services                   0    10 212 Ko
svchost.exe                   2288 Services                   0     7 648 Ko
NVDisplay.Container.exe       2316 Services                   0    20 148 Ko
svchost.exe                   2340 Services                   0    16 604 Ko
svchost.exe                   2392 Services                   0    10 520 Ko
atiesrxx.exe                  2492 Services                   0     5 868 Ko
svchost.exe                   2500 Services                   0    20 104 Ko
svchost.exe                   2516 Services                   0    12 876 Ko
svchost.exe                   2612 Services                   0    15 948 Ko
svchost.exe                   2648 Services                   0     8 372 Ko
svchost.exe                   2744 Services                   0     8 712 Ko
svchost.exe                   2920 Services                   0     5 416 Ko
svchost.exe                   2928 Services                   0    13 496 Ko
svchost.exe                   2936 Services                   0     7 480 Ko
Memory Compression            3028 Services                   0    39 532 Ko
svchost.exe                   3068 Services                   0     8 180 Ko
svchost.exe                   1688 Services                   0     7 904 Ko
svchost.exe                   2420 Services                   0     9 236 Ko
svchost.exe                   3208 Services                   0    14 928 Ko
svchost.exe                   3340 Services                   0     6 396 Ko
svchost.exe                   3348 Services                   0     9 908 Ko
svchost.exe                   3476 Services                   0     7 220 Ko
svchost.exe                   3560 Services                   0    19 840 Ko
AsusOptimization.exe          3628 Services                   0     6 404 Ko
svchost.exe                   3636 Services                   0    14 248 Ko
spoolsv.exe                   3816 Services                   0    16 012 Ko
svchost.exe                   3928 Services                   0    17 072 Ko
svchost.exe                   3972 Services                   0     7 376 Ko
taskhostw.exe                 3536 Services                   0    50 036 Ko
svchost.exe                   3848 Services                   0     6 616 Ko
AsusLinkNearExt.exe           4168 Services                   0     2 972 Ko
AsusLinkRemote.exe            4184 Services                   0    13 144 Ko
ArmouryCrateControlInterf     4192 Services                   0    10 564 Ko
AsusLinkNear.exe              4208 Services                   0    10 620 Ko
gameinputsvc.exe              4216 Services                   0     4 796 Ko
AsusSoftwareManager.exe       4224 Services                   0    20 564 Ko
ArmouryCrate.Service.exe      4240 Services                   0    42 844 Ko
svchost.exe                   4268 Services                   0     7 460 Ko
svchost.exe                   4280 Services                   0    19 572 Ko
AsusSystemDiagnosis.exe       4288 Services                   0     9 704 Ko
svchost.exe                   4296 Services                   0    22 908 Ko
AsusSystemAnalysis.exe        4304 Services                   0    13 984 Ko
DtsApo4Service.exe            4316 Services                   0    11 656 Ko
mfemms.exe                    4344 Services                   0    14 172 Ko
RtkAudUService64.exe          4380 Services                   0    10 068 Ko
RtkBtManServ.exe              4388 Services                   0     8 128 Ko
ROGLiveService.exe            4396 Services                   0    18 192 Ko
svchost.exe                   4408 Services                   0     5 712 Ko
svchost.exe                   4416 Services                   0     5 304 Ko
svchost.exe                   4428 Services                   0    22 060 Ko
LightingService.exe           4444 Services                   0    22 656 Ko
ModuleCoreService.exe         4464 Services                   0    54 560 Ko
svchost.exe                   4472 Services                   0     7 644 Ko
svchost.exe                   4480 Services                   0    23 376 Ko
svchost.exe                   4520 Services                   0    33 012 Ko
PEFService.exe                4528 Services                   0    18 568 Ko
servicehost.exe               4556 Services                   0    20 440 Ko
svchost.exe                   4712 Services                   0     8 280 Ko
GamingServicesNet.exe         4744 Services                   0     4 448 Ko
GamingServices.exe            4956 Services                   0    25 188 Ko
svchost.exe                   4896 Services                   0    13 688 Ko
svchost.exe                   5464 Services                   0     4 948 Ko
svchost.exe                   5692 Services                   0    11 628 Ko
MMSSHOST.exe                  6576 Services                   0    49 492 Ko
mfevtps.exe                   6820 Services                   0    13 292 Ko
WmiPrvSE.exe                  6828 Services                   0    10 268 Ko
ProtectedModuleHost.exe       7092 Services                   0    15 164 Ko
svchost.exe                   7100 Services                   0     4 344 Ko
svchost.exe                   7340 Services                   0    12 188 Ko
dllhost.exe                   8112 Services                   0    10 280 Ko
MfeAVSvc.exe                  6196 Services                   0    48 336 Ko
mcapexe.exe                   7760 Services                   0     3 224 Ko
McCSPServiceHost.exe          7804 Services                   0    16 472 Ko
svchost.exe                   8196 Services                   0    11 432 Ko
svchost.exe                   8968 Services                   0    31 108 Ko
mcshield.exe                  9200 Services                   0   300 604 Ko
svchost.exe                   9580 Services                   0     6 684 Ko
svchost.exe                   2004 Services                   0    25 216 Ko
svchost.exe                   4324 Services                   0     7 568 Ko
svchost.exe                  10280 Services                   0    21 856 Ko
GoogleCrashHandler.exe       12068 Services                   0     1 580 Ko
GoogleCrashHandler64.exe     12088 Services                   0     1 388 Ko
svchost.exe                   9928 Services                   0     9 748 Ko
McSmtFwk.exe                 15248 Services                   0     2 692 Ko
svchost.exe                  11380 Services                   0     5 488 Ko
SecurityHealthService.exe    13032 Services                   0    19 576 Ko
McAWFwk.exe                  15488 Services                   0     9 768 Ko
svchost.exe                   1656 Services                   0    10 868 Ko
svchost.exe                  10604 Services                   0    18 892 Ko
svchost.exe                  14100 Services                   0    17 732 Ko
SgrmBroker.exe               13684 Services                   0     6 100 Ko
svchost.exe                  14688 Services                   0    10 952 Ko
svchost.exe                  18876 Services                   0    11 640 Ko
svchost.exe                  16764 Services                   0     8 600 Ko
svchost.exe                  18060 Services                   0    11 384 Ko
svchost.exe                  11840 Services                   0    19 236 Ko
svchost.exe                  11964 Services                   0     8 360 Ko
OfficeClickToRun.exe          5080 Services                   0    58 864 Ko
AppVShNotify.exe             16700 Services                   0     7 040 Ko
SearchIndexer.exe            21272 Services                   0    55 024 Ko
atkexComSvc.exe               2008 Services                   0     9 804 Ko
svchost.exe                   2984 Services                   0     6 196 Ko
WmiPrvSE.exe                 18196 Services                   0    19 296 Ko
svchost.exe                   9892 Services                   0    19 968 Ko
csrss.exe                    13044 Console                    4     5 552 Ko
winlogon.exe                  7684 Console                    4    10 108 Ko
fontdrvhost.exe              22456 Console                    4     4 216 Ko
dwm.exe                      21624 Console                    4    68 500 Ko
svchost.exe                  14000 Services                   0     7 212 Ko
svchost.exe                  22164 Services                   0    17 412 Ko
gameinputsvc.exe             12048 Console                    4     4 252 Ko
NVDisplay.Container.exe      15648 Console                    4    45 716 Ko
unsecapp.exe                  5832 Services                   0     6 628 Ko
sihost.exe                   11116 Console                    4    29 348 Ko
svchost.exe                  17428 Console                    4     8 248 Ko
svchost.exe                  15104 Console                    4    30 976 Ko
AsusOptimizationStartupTa    15936 Console                    4    14 104 Ko
uihost.exe                   14328 Console                    4    18 576 Ko
svchost.exe                  17276 Services                   0     9 984 Ko
svchost.exe                   2224 Console                    4    38 840 Ko
GoogleUpdate.exe              6956 Services                   0     1 972 Ko
ArmourySocketServer.exe       3992 Console                    4    12 944 Ko
taskhostw.exe                12284 Console                    4    16 964 Ko
svchost.exe                   1776 Services                   0     7 172 Ko
asus_framework.exe           12996 Console                    4    34 316 Ko
AsusUpdate.exe               16656 Services                   0     2 008 Ko
P508PowerAgent.exe           14308 Console                    4    32 068 Ko
MicrosoftEdgeUpdate.exe      21884 Services                   0     1 896 Ko
svchost.exe                   7800 Services                   0     7 108 Ko
McUICnt.exe                  19880 Console                    4    32 872 Ko
explorer.exe                 12384 Console                    4   135 996 Ko
svchost.exe                   2656 Console                    4    17 776 Ko
StartMenuExperienceHost.e    12156 Console                    4    75 848 Ko
RuntimeBroker.exe             6764 Console                    4    26 264 Ko
ctfmon.exe                    6464 Console                    4    14 368 Ko
SearchUI.exe                 22484 Console                    4   149 168 Ko
RuntimeBroker.exe            22160 Console                    4    28 560 Ko
RuntimeBroker.exe            10616 Console                    4    20 608 Ko
UserOOBEBroker.exe            1684 Console                    4     8 640 Ko
LockApp.exe                  16672 Console                    4    49 368 Ko
RuntimeBroker.exe            12812 Console                    4    32 784 Ko
asus_framework.exe           17748 Console                    4    32 080 Ko
ArmourySwAgent.exe            9916 Console                    4    24 660 Ko
svchost.exe                   7816 Services                   0    10 216 Ko
ArmouryCrate.UserSessionH    14668 Console                    4    44 744 Ko
conhost.exe                   9148 Console                    4    11 012 Ko
svchost.exe                  10108 Services                   0     5 508 Ko
SettingSyncHost.exe          15276 Console                    4    30 056 Ko
SystemSettings.exe           15968 Console                    4    59 172 Ko
ApplicationFrameHost.exe     15908 Console                    4    38 892 Ko
svchost.exe                  15792 Services                   0    38 804 Ko
AsusOSD.exe                  22420 Console                    4    13 364 Ko
svchost.exe                  12028 Services                   0     5 816 Ko
WinStore.App.exe             15280 Console                    4    48 120 Ko
RuntimeBroker.exe            11184 Console                    4     9 000 Ko
RtkAudUService64.exe         16360 Console                    4    11 896 Ko
ArmouryCrateKeyControl.ex     1940 Console                    4    45 684 Ko
smartscreen.exe              10424 Console                    4    28 624 Ko
SearchProtocolHost.exe       13828 Console                    4     7 900 Ko
SearchFilterHost.exe           860 Services                   0     6 288 Ko
ModuleCoreService.exe        19240 Console                    4    36 100 Ko
conhost.exe                  17444 Console                    4    11 120 Ko
svchost.exe                  12976 Services                   0     8 304 Ko
backgroundTaskHost.exe       10688 Console                    4    31 548 Ko
YourPhone.exe                14724 Console                    4    67 192 Ko
RuntimeBroker.exe            19052 Console                    4    21 236 Ko
svchost.exe                   1148 Console                    4    22 316 Ko
RuntimeBroker.exe            20032 Console                    4    28 352 Ko
RuntimeBroker.exe            19900 Console                    4    36 152 Ko
ShellExperienceHost.exe      14488 Console                    4    58 028 Ko
audiodg.exe                  19828 Services                   0    61 508 Ko
RuntimeBroker.exe            21764 Console                    4    34 244 Ko
Discord.exe                  13020 Console                    4    71 600 Ko
Discord.exe                   9392 Console                    4   131 808 Ko
Discord.exe                  21516 Console                    4    25 684 Ko
MyASUS.exe                   23052 Console                    4    97 396 Ko
AsusSoftwareManagerAgent.    23352 Console                    4    36 680 Ko
SysTray.exe                  23392 Console                    4    36 220 Ko
svchost.exe                  22820 Services                   0    15 648 Ko
Discord.exe                  22804 Console                    4    13 684 Ko
Discord.exe                  16316 Console                    4   218 224 Ko
Discord.exe                  22896 Console                    4    18 552 Ko
AsusLinkRemoteAgent.exe      23756 Console                    4    37 608 Ko
SecurityHealthSystray.exe    24092 Console                    4     8 976 Ko
OneDrive.exe                 24212 Console                    4    71 088 Ko
EpicGamesLauncher.exe        24416 Console                    4   126 340 Ko
Teams.exe                    16880 Console                    4   210 144 Ko
RuntimeBroker.exe            23924 Console                    4    17 796 Ko
SearchProtocolHost.exe       24740 Services                   0    17 756 Ko
Teams.exe                    25148 Console                    4    92 164 Ko
ACMON.exe                    25208 Console                    4    20 480 Ko
Teams.exe                    25472 Console                    4    57 308 Ko
Teams.exe                    24872 Console                    4   482 372 Ko
UnrealCEFSubProcess.exe      14576 Console                    4    35 768 Ko
setup.exe                    26044 Services                   0     2 124 Ko
setup.exe                    26164 Services                   0     4 772 Ko
msedge.exe                   26256 Console                    4   129 816 Ko
msedge.exe                   26312 Console                    4     6 832 Ko
msedge.exe                   26552 Console                    4   115 096 Ko
msedge.exe                   26596 Console                    4    38 672 Ko
msedge.exe                   26128 Console                    4    45 720 Ko
msedge.exe                   26120 Console                    4   101 616 Ko
Teams.exe                    25960 Console                    4   137 344 Ko
Teams.exe                     2436 Console                    4   137 168 Ko
msedge.exe                   27624 Console                    4    84 008 Ko
msedge.exe                   27632 Console                    4    96 072 Ko
msedge.exe                   27252 Console                    4    63 212 Ko
msedge.exe                   27324 Console                    4    37 544 Ko
msedge.exe                   21060 Console                    4   153 040 Ko
msedge.exe                    6560 Console                    4    81 316 Ko
msedge.exe                   12192 Console                    4    23 740 Ko
RuntimeBroker.exe             9768 Console                    4    20 564 Ko
svchost.exe                  21056 Services                   0    21 264 Ko
cmd.exe                      20428 Console                    4     3 996 Ko
conhost.exe                   3224 Console                    4    18 892 Ko
svchost.exe                  21732 Services                   0     6 716 Ko
tasklist.exe                  7332 Console                    4     9 944 Ko
```
---
    * svchost.exe                     84 Services                   0     2 864 Ko
    Il sert d'hôte pour les fonctionnalités de bibliothèques de liens dynamiques.
    
---
    * csrss.exe                    11628 Console                    3     5 540 Ko
    Le sous - système d'exécution du client, ou csrss.exe, est un composant de la famille de systèmes d'exploitation Windows NT qui fournit le côté utilisateur du sous - système Win32 en mode utilisateur. Il est inclus dans Windows NT 3.1 et les versions ultérieures.
    
---
    * explorer.exe                  9788 Console                    3   120 728 Ko
    Le gestionnaire permet, notamment, d'afficher et de modifier le nom des fichiers et des dossiers, de manipuler les fichiers et les dossiers, d'ouvrir les fichiers de données, et de lancer les programmes.
    
---
    * smss.exe                       496 Services                   0       932 Ko
    Il lance autochk.exe pour vérifier le ou les différent systèmes de fichiers, puis après cette vérification, il crée les variables d'environnement et démarre.
    
---
    * lsass.exe                      912 Services                   0    17 652 Ko
    Il assure l'identification des utilisateurs.

* Il faut taper dans l’invite de commande taskmgr qui ouvrira le gestionnaire de tâche -> Détails -> Sélectionner des colonnes -> Elever 

##### Network
```
* PS C:\Users\Muralee> Get-NetAdapter
Name                      InterfaceDescription                    ifIndex Status       MacAddress             LinkSpeed
----                      --------------------                    ------- ------       ----------             ---------
Connexion réseau Bluet... Bluetooth Device (Personal Area Netw...      16 Disconnected 08-D2-3E-62-C0-E0         3 Mbps
Wi-Fi                     Intel(R) Wireless-AC 9462                    11 Up           08-D2-3E-62-C0-DC       200 Mbps
VirtualBox Host-Only N... VirtualBox Host-Only Ethernet Adapter         7 Up           0A-00-27-00-00-07         1 Gbps
```
* TCP:

```
C:\Windows\system32>netstat -p tcp -a -n -b

Connexions actives

  Proto  Adresse locale         Adresse distante       État
  TCP    0.0.0.0:135            0.0.0.0:0              LISTENING
  RpcEptMapper
 [svchost.exe]
  TCP    0.0.0.0:445            0.0.0.0:0              LISTENING
 Impossible d’obtenir les informations de propriétaire
  TCP    0.0.0.0:808            0.0.0.0:0              LISTENING
 [OneApp.IGCC.WinService.exe]
  TCP    0.0.0.0:5040           0.0.0.0:0              LISTENING
  CDPSvc
 [svchost.exe]
  TCP    0.0.0.0:7680           0.0.0.0:0              LISTENING
 Impossible d’obtenir les informations de propriétaire
  TCP    0.0.0.0:9001           0.0.0.0:0              LISTENING
 Impossible d’obtenir les informations de propriétaire
  TCP    0.0.0.0:49664          0.0.0.0:0              LISTENING
 [lsass.exe]
  TCP    0.0.0.0:49665          0.0.0.0:0              LISTENING
 Impossible d’obtenir les informations de propriétaire
  TCP    0.0.0.0:49666          0.0.0.0:0              LISTENING
  EventLog
 [svchost.exe]
  TCP    0.0.0.0:49667          0.0.0.0:0              LISTENING
  Schedule
 [svchost.exe]
  TCP    0.0.0.0:49668          0.0.0.0:0              LISTENING
 [spoolsv.exe]
  TCP    0.0.0.0:49670          0.0.0.0:0              LISTENING
 Impossible d’obtenir les informations de propriétaire
  TCP    0.0.0.0:57621          0.0.0.0:0              LISTENING
 [Spotify.exe]
  TCP    0.0.0.0:58759          0.0.0.0:0              LISTENING
 [Spotify.exe]
  TCP    127.0.0.1:12025        0.0.0.0:0              LISTENING
 Impossible d’obtenir les informations de propriétaire
  TCP    127.0.0.1:12110        0.0.0.0:0              LISTENING
 Impossible d’obtenir les informations de propriétaire
  TCP    127.0.0.1:12119        0.0.0.0:0              LISTENING
 Impossible d’obtenir les informations de propriétaire
  TCP    127.0.0.1:12143        0.0.0.0:0              LISTENING
 Impossible d’obtenir les informations de propriétaire
  TCP    127.0.0.1:12465        0.0.0.0:0              LISTENING
 Impossible d’obtenir les informations de propriétaire
  TCP    127.0.0.1:12563        0.0.0.0:0              LISTENING
 Impossible d’obtenir les informations de propriétaire
  TCP    127.0.0.1:12993        0.0.0.0:0              LISTENING
 Impossible d’obtenir les informations de propriétaire
  TCP    127.0.0.1:12995        0.0.0.0:0              LISTENING
 Impossible d’obtenir les informations de propriétaire
  TCP    127.0.0.1:27275        0.0.0.0:0              LISTENING
 Impossible d’obtenir les informations de propriétaire
  TCP    192.168.1.50:139       0.0.0.0:0              LISTENING
 Impossible d’obtenir les informations de propriétaire
  TCP    192.168.1.50:58725     40.67.254.36:443       ESTABLISHED
  WpnService
 [svchost.exe]
  TCP    192.168.1.50:58770     35.190.242.206:4070    ESTABLISHED
 [Spotify.exe]
  TCP    192.168.1.50:58775     35.186.224.47:443      ESTABLISHED
 [Spotify.exe]
  TCP    192.168.1.50:58808     5.62.53.11:80          ESTABLISHED
 Impossible d’obtenir les informations de propriétaire
  TCP    192.168.1.50:58859     77.234.45.64:80        ESTABLISHED
 Impossible d’obtenir les informations de propriétaire
  TCP    192.168.1.50:59198     91.121.38.247:443      ESTABLISHED
 [opera.exe]
  TCP    192.168.1.50:59286     92.222.75.23:443       ESTABLISHED
 [opera.exe]
  TCP    192.168.1.50:59624     69.173.144.158:443     TIME_WAIT
  TCP    192.168.1.50:59631     151.101.122.49:443     ESTABLISHED
 [Spotify.exe]
  TCP    192.168.1.50:59653     51.104.167.255:443     TIME_WAIT
  TCP    192.168.1.50:59658     20.54.24.246:443       ESTABLISHED
 Impossible d’obtenir les informations de propriétaire
  TCP    192.168.1.50:59659     51.104.167.255:443     ESTABLISHED
 Impossible d’obtenir les informations de propriétaire
  TCP    192.168.1.50:59660     51.104.167.255:443     ESTABLISHED
 Impossible d’obtenir les informations de propriétaire
  TCP    192.168.1.50:59661     51.104.167.255:443     ESTABLISHED
 Impossible d’obtenir les informations de propriétaire
  TCP    192.168.1.50:59665     213.19.162.21:443      ESTABLISHED
 [Spotify.exe]
  TCP    192.168.1.50:59666     52.109.68.21:443       TIME_WAIT
  TCP    192.168.1.50:59667     213.19.162.57:443      ESTABLISHED
 [Spotify.exe]
  TCP    192.168.1.50:59668     52.109.68.21:443       TIME_WAIT
  TCP    192.168.1.50:59670     37.157.2.234:443       ESTABLISHED
 [Spotify.exe]
  TCP    192.168.1.50:59671     69.173.144.139:443     ESTABLISHED
 [Spotify.exe]
  TCP    192.168.1.50:59672     35.244.147.96:443      ESTABLISHED
 [Spotify.exe]
  TCP    192.168.1.50:59673     52.28.82.26:443        ESTABLISHED
 [Spotify.exe]
  TCP    192.168.1.50:59674     52.57.167.187:443      ESTABLISHED
 [Spotify.exe]
  TCP    192.168.1.50:59675     35.186.193.173:443     ESTABLISHED
 [Spotify.exe]
  TCP    192.168.1.50:59676     35.156.158.150:443     ESTABLISHED
 [Spotify.exe]
  TCP    192.168.1.50:59677     23.57.5.124:443        ESTABLISHED
 [Spotify.exe]
  TCP    192.168.1.50:59678     85.114.159.93:443      ESTABLISHED
 [Spotify.exe]
  TCP    192.168.1.50:59679     69.173.144.139:443     ESTABLISHED
 [Spotify.exe]
  TCP    192.168.1.50:59680     52.19.189.90:443       ESTABLISHED
 [Spotify.exe]
  TCP    192.168.1.50:59681     10.33.19.123:7680      SYN_SENT
 Impossible d’obtenir les informations de propriétaire
  TCP    192.168.1.50:59682     10.33.18.87:7680       SYN_SENT
 Impossible d’obtenir les informations de propriétaire
  TCP    192.168.56.1:139       0.0.0.0:0              LISTENING
 Impossible d’obtenir les informations de propriétaire
 ```
 * Fonction TCP : 
     * WinStore.App.exe : Windows Store
     * svchost.exe :Il sert d'hôte pour les fonctionnalités de bibliothèques de liens 
     * opera.exe : Navigateur OperaGX
     * Discord.exe : Plateforme de communication
     * SystemSettings.exe : Paramètres PC de Windows
     * SearchUI.exe : SearchUI.exe active l'interface utilisateur de recherche de l'assistant de recherche Microsoft Cortana. 
 
 * UDP : 
```
C:\Windows\system32>netstat -p udp -a -n -b

Connexions actives

  Proto  Adresse locale         Adresse distante       État
  UDP    0.0.0.0:123            *:*
  W32Time
 [svchost.exe]
  UDP    0.0.0.0:500            *:*
  IKEEXT
 [svchost.exe]
  UDP    0.0.0.0:4500           *:*
  IKEEXT
 [svchost.exe]
  UDP    0.0.0.0:5050           *:*
  CDPSvc
 [svchost.exe]
  UDP    0.0.0.0:5353           *:*
 [Spotify.exe]
  UDP    0.0.0.0:5353           *:*
 [Spotify.exe]
  UDP    0.0.0.0:5353           *:*
 [Spotify.exe]
  UDP    0.0.0.0:5353           *:*
 [Spotify.exe]
  UDP    0.0.0.0:5353           *:*
 [Spotify.exe]
  UDP    0.0.0.0:5353           *:*
  Dnscache
 [svchost.exe]
  UDP    0.0.0.0:5353           *:*
 [Spotify.exe]
  UDP    0.0.0.0:5353           *:*
 [Spotify.exe]
  UDP    0.0.0.0:5355           *:*
  Dnscache
 [svchost.exe]
  UDP    0.0.0.0:50739          *:*
 [opera.exe]
  UDP    0.0.0.0:52660          *:*
 Impossible d’obtenir les informations de propriétaire
  UDP    0.0.0.0:54196          *:*
 [opera.exe]
  UDP    0.0.0.0:57412          *:*
 [Spotify.exe]
  UDP    0.0.0.0:57413          *:*
 [Spotify.exe]
  UDP    0.0.0.0:57621          *:*
 [Spotify.exe]
  UDP    127.0.0.1:1900         *:*
  SSDPSRV
 [svchost.exe]
  UDP    127.0.0.1:52656        *:*
  iphlpsvc
 [svchost.exe]
  UDP    127.0.0.1:52658        *:*
 Impossible d’obtenir les informations de propriétaire
  UDP    127.0.0.1:53698        *:*
  SSDPSRV
 [svchost.exe]
  UDP    127.0.0.1:63132        *:*
 [WINWORD.EXE]
  UDP    192.168.1.50:137       *:*
 Impossible d’obtenir les informations de propriétaire
  UDP    192.168.1.50:138       *:*
 Impossible d’obtenir les informations de propriétaire
  UDP    192.168.1.50:1900      *:*
  SSDPSRV
 [svchost.exe]
  UDP    192.168.1.50:2177      *:*
  QWAVE
 [svchost.exe]
  UDP    192.168.1.50:53697     *:*
  SSDPSRV
 [svchost.exe]
  UDP    192.168.56.1:137       *:*
 Impossible d’obtenir les informations de propriétaire
  UDP    192.168.56.1:138       *:*
 Impossible d’obtenir les informations de propriétaire
  UDP    192.168.56.1:1900      *:*
  SSDPSRV
 [svchost.exe]
  UDP    192.168.56.1:2177      *:*
  QWAVE
 [svchost.exe]
  UDP    192.168.56.1:53696     *:*
  SSDPSRV
 [svchost.exe]
 ```
 * Function UDP : 
     * svchost.exe : Il sert d'hôte pour les fonctionnalités de bibliothèques de liens dynamiques.
     * opera.exe : Navigateur OperaGX
     * WINWORD.EXE : logiciel de traitement de texte

----------------

###### Scripting

Ceci est le script qui nous permets de  savoir les informations de notre pc mais aussi calcule  et affiche le débit maximum en download et upload vers internet.
--

```
#script qui permet d'afficher un résumé de l'os
#Auteur : Muraleedaran Mukesh
#Date : 24/10/2020

Write-Output "Nom de l'ordinateur : $env:computername"
$Os = (Get-WMIObject win32_operatingsystem).name
$Os = $Os.split("|")[0]
Write-Output "Os : $Os"
$OSVersion = (Get-WMIObject win32_operatingsystem).version
Write-Output "Os Version : $OsVersion"
$Date = (Get-CimInstance Win32_OperatingSystem).LastBootUpTime 
Write-Output "Date et heure d'allumage : $Date"
$criteria = "Type='software' and IsAssigned=1 and IsHidden=0 and IsInstalled=0"
$searcher = (New-Object -COM Microsoft.Update.Session).CreateUpdateSearcher()
$updates = $searcher.Search($criteria).Updates
if ($updates.Count -ne 0) {
    $osUpdated = "Le systeme est t'il a jour : Non"
}
else {
    $osUpdated = "Le systeme est t'il a jour : Oui"
}
Write-Output "est-ce que l'os est a jour : $osUpdated"

Write-Output " "

$RamTotale = [STRING]((Get-WmiObject -Class Win32_ComputerSystem ).TotalPhysicalMemory / 1GB)
Write-Output "Ram totale : $RamTotale"
$RamLibre = (Get-CIMInstance Win32_OperatingSystem).FreePhysicalMemory
Write-Output "RAM disponible : $RamLibre Go"
$RamUtiliser = [String]((Get-WmiObject -Class Win32_OperatingSystem).FreePhysicalMemory / 1MB)
Write-Output "Ram utiliser : $RamUtiliser Go"


$DiskTotal = [Math]::Round((Get-Volume -DriveLetter 'C').Size / 1GB)
$DiskUse = [Math]::Round((Get-Volume -DriveLetter 'C').SizeRemaining / 1GB)
Write-Output "Espace disque utiliser : $DiskUse Go"
$DiskDispo = ($DiskTotal - $DiskUse)
Write-Output "Espace disque disponible : $DiskDispo Go"

Write-Output " "

$ip = (Test-Connection -ComputerName $env:computername -count 1).IPV4Address.ipaddressTOstring
Write-Output "Ip principale : $ip"
$connection = (Test-Connection -ComputerName "8.8.8.8" -Count 4  | measure-Object -Property ResponseTime -Average).average
Write-Output " - Ping : $connection ms"
$DownloadSpeed = [math]::Round($SpeedtestResults.download.bandwidth / 1000000 * 8, 2)
$UploadSpeed = [math]::Round($SpeedtestResults.upload.bandwidth / 1000000 * 8, 2)
Write-Output " - download speed : $DownloadSpeed Mbit/s"
Write-Output " - upload speed : $UploadSpeed Mbit/s"

Write-Output " "

$Users = (Get-WmiObject Win32_UserAccount).Name 
Write-Output "La liste des utilisateurs : $Users"


```


* PS C:\Users\Muralee> cd desktop
* PS C:\Users\Muralee\desktop> cd script
* PS C:\Users\Muralee\desktop\script> .\zeb

```
Nom de l'ordinateur : LAPTOP-PQ1V32RL
Os : Microsoft Windows 10 Famille
Os Version : 10.0.18363
Date et heure d'allumage : 10/23/2020 17:15:29
est-ce que l'os est a jour : Le systeme est t'il a jour : Oui

RAM

Ram totale : 15.4195327758789
RAM disponible : 9722016 Go
Ram utiliser : 9.27170944213867 Go

Disk

Espace disque utiliser : 114 Go
Espace disque disponible : 105 Go

Ip principale : 192.168.0.35
 - Ping : 18.75 ms
 - download speed : 0 Mbit/s
 - upload speed : 0 Mbit/s


```

----------------------------


Ceci est le deuxieme script qui permets de mettre en veille ou d'éteindre notre pc après un certain temps.
--

```

#Script qui permet, en fonction d'arguments qui lui sont passés :
#Auteur : Muraleedaran Mukesh
#Date : 25/10/2020

if ($args[0] -match "lock") {
    Start-Sleep -Seconds $args[1]              
    rundll32.exe user32.dll, LockWorkStation
}
elseif ($args -match "shutdown") {
    shutdown /s /t $args[1]                     
}
else {
    echo "wrong arguments"                     
}

```
  Ici, le "if" permet de verouillez l'ecran au bout d'un temps entré en argument
  Puis, Le "eslsif" sert à eteindre l'ordinateur au bout d'un temps entré en argument
  Enfin, Le "else" permet de dire que l'argument entré est respecté ou non
_________










# III. Gestion de softs

### Expliquer l'intérêt de l'utilisation d'un gestionnaire de paquets

Le gestionnaire de paquets est un outil permettant de gérer les paquets et leurs mises à jour avec un objectif principal : garder la cohérence entre toutes les dépendances.

Son job se divise en deux parties :
--
Récupérer les librairies (les morceaux de code qui ont déjà été développées par d’autres personnes : communauté, fournisseur de services , etc. ) et les intégrer dans votre projet pour que vous puissiez les utiliser.
Maintenir les librairies dans le temps, de savoir si et quand il faut les mettre à jour… Eviter d’avoir des incompatibilités !
Les gestionnaires de paquets de développement les plus connus à ce jour sont NPM, Nuget, Maven et Gradle. Certains gestionnaires de paquets vont d’ailleurs plus loin en proposant d’autres fonctionnalités comme la gestion de builds et de profils (type développement, type recette) et le paramétrage d’environnement pour l’applicatif.

Les avantages des gestionnaires de paquets
--
Les gestionnaires de paquets n’ont que des avantages à être utiliser :

Configuration automatique : avec le gestionnaire de paquets, on peut configurer par exemple automatiquement les versions de mises à jour de sécurité des paquets, et demander une autorisation du développeur pour les mises à jour majeures.
Eviter les duplications : Ça permet aussi d’éviter les duplications ( exemple : si deux librairies qu’on utilise, utilise la même autre librairie, ça évite d’avoir à télécharger deux fois si jamais on l’avait téélchargé en dur dans notre projet.)
Optimiser et gagner du temps dans son projet : lorsqu’on a des milliers de librairies, ça peut vite devenir un tas de nœud ! pas de solution parfaite pour déméler tout ce bazar J
C’est simple, avec le gestionnaire de paquets, je dis : « j’ai besoin de la librairie XX, merci de me récupérer tout ce dont cette librairie a besoin et de surveiller son évolution »

### Utiliser un gestionnaire de paquet propres à votre OS pour

* lister tous les paquets déjà installés

C:\Users\Muralee> choco list
--
```
Chocolatey v0.10.15
subtitleedit.portable 3.5.11 [Approved] Downloads cached for licensed users
maxthon.install 4.9.4.1000 [Approved] Downloads cached for licensed users
maxthon 4.9.4.1000 [Approved]
BatteryBar 3.6.6 [Approved] Downloads cached for licensed users
milton 1.9.1 [Approved]
milton.portable 1.9.1 [Approved]
maxthon.commandline 5.3.8.2000 [Approved] Downloads cached for licensed users - Possibly broken for FOSS users (due to original download location changes by vendor)
docfetcher 1.1.22 [Approved] Downloads cached for licensed users - Possibly broken for FOSS users (due to original download location changes by vendor)
chrono-chrome 0.10.0 [Approved]
lgbridge 1.2.56 [Approved] Downloads cached for licensed users - Possibly broken for FOSS users (due to original download location changes by vendor)
dacia-media-nav-toolbox 3.18.5.740218 [Approved] Downloads cached for licensed users
vswhere 2.8.4 [Approved]
javaruntime 8.0.231 [Approved]
nunit-extension-vs-project-loader 3.8.0 [Approved]
windowsphonedevelopertools 1.0 [Approved]
PowerShell 5.1.14409.20180811 [Approved]
vidcutter 6.0.0.1 [Approved] Downloads cached for licensed users
lastpass-for-applications 4.35.0 [Approved] Downloads cached for licensed users
dropkick.core 0.4.13.0 [Approved]
dropkick 0.4.13.0 [Approved]
clonespy 3.43 [Approved] Downloads cached for licensed users
ipfs-mount 0.3.3 [Approved]
netcat 1.12 [Approved]
adobereader-update 18.011.20058 [Approved] Downloads cached for licensed users - Possibly broken for FOSS users (due to original download location changes by vendor)
DeleGate 9.9.7 [Approved]
DarkRoom 0.8.1
msxml4.sp3 4.30.2100 [Approved] Downloads cached for licensed users
GnuWin 0.6.3.1 - Possibly broken
ctags 5.8.1
PomoTime 1.8
signcode.install 1.0.4 [Approved] - Possibly broken
go-repo-utils 0.0.17 [Approved]
xnews 5.4.25 - Possibly broken
onedrivebully 1.2 [Approved]
cmdaliases 1.0.0.1
backupper-standard 5.3.0 [Approved] Downloads cached for licensed users
hydrairc 0.3.165.1
specflow 1.8.1
baretail 3.50.0.20120226 [Approved]
brewtarget 2.3.0.20160816 [Approved] Downloads cached for licensed users
backupper-server 5.3.0 [Approved] - Possibly broken
sqlnotebook 0.6.0 [Approved] Downloads cached for licensed users
initool 0.9.0 [Approved] Downloads cached for licensed users
zip.template 1.0.0 [Approved]
marker 0.0.6.0 [Approved]
wcat 6.4.0 [Approved]
eazfuscator.net 3.3 [Approved]
tfsSidekicks2010 3.1.1
WinVDIG 1.0.5
dotnet-verification-tool 4.6.01528.0 [Approved] Downloads cached for licensed users
CoffeeScript 1.3.1.01
real-netstat 3.1 [Approved] Downloads cached for licensed users
clojure.clr 1.3.0
xboot 1.0.14 [Approved] Downloads cached for licensed users
winhotkey 0.70.0.1 [Approved] Downloads cached for licensed users
mobipocket.reader 6.2
evince 2.32.0.145001 [Approved] Downloads cached for licensed users
viper4windows 1.0.5.1 [Approved] Downloads cached for licensed users
factor 0.97 [Approved] Downloads cached for licensed users
59 packages found.
```

* déterminer la provenance des paquets

C:\Users\Muralee> choco list -verbose --trace
--
```
{...}
subtitleedit.portable 3.5.11 [Approved] Downloads cached for licensed users
 Title: Subtitle Edit (Portable) | Published: 27/10/2019
 {...}
 Chocolatey Package Source: https://github.com/echizenryoma/Chocolatey-Package/tree/master/subtitleedit
 Package Checksum: '9VmfJ1caqwJmTo61TII2hdRLUj7u6SfePOMnvL3eAyTquK1iNFc5vZpJ1kBv5sdTbJLq/bf08Fj5WOTdsepgyA==' (SHA512)
 Software Site: http://www.nikse.dk/SubtitleEdit/
 Software Source: https://github.com/SubtitleEdit/subtitleedit
{...}

maxthon.install 4.9.4.1000 [Approved] Downloads cached for licensed users
 Title: Maxthon (Install) | Published: 10/10/2016
{...}
 Chocolatey Package Source: https://github.com/ferventcoder/chocolatey-packages/
 Package Checksum: 'DLGpCupfh4ui2fZwmcv36IHVv7ZKLjH8vlkFGVfCv0imyw7BXNqtb7T/Dvc4/h6Q0ejNOD9RRwSZeMAh2A/FRg==' (SHA512)
 Software Site: http://www.maxthon.com/
{...}

maxthon 4.9.4.1000 [Approved]
 Title: Maxthon | Published: 10/10/2016
 {...}
 Chocolatey Package Source: https://github.com/ferventcoder/chocolatey-packages/
 Package Checksum: 'V1VdtAOoapp1kdrVUOFVvKcyGvDcaoJINdsNP+BxKaZ03WjIaDROqAoPlz0hxWawfobp6x2Z5ybtOsktLfL4Ew==' (SHA512)
 Software Site: http://www.maxthon.com/
{...}

BatteryBar 3.6.6 [Approved] Downloads cached for licensed users
 Title: BatteryBar (Install) | Published: 10/10/2016
{...}
 Chocolatey Package Source: n/a
 Package Checksum: 'V+h5G48U8AGtOMHbz5DSU3tHxJ3ke3syPlxWu9bJoLJoGcUH9obhpC+rCuiHmfApMOvhE3VVul5iPpexdDgVBg==' (SHA512)
 Software Site: https://batterybarpro.com/
{...}

milton 1.9.1 [Approved]
 Title: Milton | Published: 27/10/2019
 {...}
 Chocolatey Package Source: https://github.com/chtof/chocolatey-packages/tree/master/automatic/milton.install
 Package Checksum: 'y4Un4KyKb5TBoHHdQ96XJK5u0Dt01dvSGwxoNwj9EcNsxXt6VPTZzCHkUGbZsQADYtCkaoLATzqw/szXx1vBGw==' (SHA512)
{...}
 Software Site: https://github.com/serge-rgb/milton
 Software Source: https://github.com/serge-rgb/milton
{...}

milton.portable 1.9.1 [Approved]
 Title: Milton (Portable) | Published: 27/10/2019
{...}
 Chocolatey Package Source: https://github.com/chtof/chocolatey-packages/tree/master/automatic/milton.portable
 Package Checksum: 'lDROck7obDL5bKOKJELl8aVbszKEmr7zddJoEg/2ivVxTKLr/X+KFyn+TJB8QnYrGx5fLxeP/eoYOGCOhNw5oA==' (SHA512)
{...}
 Software Site: https://github.com/serge-rgb/milton
 Software Source: https://github.com/serge-rgb/milton
{...}

maxthon.commandline 5.3.8.2000 [Approved] Downloads cached for licensed users - Possibly broken for FOSS users (due to original download location changes by vendor)
 Title: Maxthon (Portable) | Published: 28/10/2019
{...}
 Chocolatey Package Source: https://github.com/chocolatey-community/chocolatey-coreteampackages/tree/master/automatic/maxthon.commandline
 Package Checksum: 'WY9fcsqLb7yTOrCs+vISBbeRoYNSNRdak+a5D5WAESgRChQFNvMWbCzJxbGAiwSTH1rcYopVYuA7LnkqocMl3g==' (SHA512)
 Software Site: http://www.maxthon.com/
 Software License: http://www.maxthon.com/user-agreement/
{...}

docfetcher 1.1.22 [Approved] Downloads cached for licensed users - Possibly broken for FOSS users (due to original download location changes by vendor)
 Title: DocFetcher (Install) | Published: 10/08/2018
{...}
 Chocolatey Package Source: https://github.com/Micha10/chocolatey/tree/master/DocFetcher
 Package Checksum: '2/x86lwsfo4Xt0w+kxP9f4ijvcaN/UXaNep4VePUd8B3UWvKiBF1VZJtbS7Srvj0IhED6Fp86OHS1GFi5+8RWQ==' (SHA512)
 Software Site: http://docfetcher.sourceforge.net/en/index.html
 Software Source: git://git.code.sf.net/p/docfetcher/code
 {...}

chrono-chrome 0.10.0 [Approved]
 Title: Chrono Download Manager for Chrome | Published: 10/08/2018
{...}
 Chocolatey Package Source: https://github.com/bcurran3/ChocolateyPackages/tree/master/chrono-chrome
 Package Checksum: 'ogq/uYBjuC4tYAnMmk1UbZ/duviYYipRFq4+BUO39EE59iAxYcnNfViHH9Eu3U0yHjUR8Wmc3m08iq2tAnRCOw==' (SHA512)
 Software Site: https://www.chronodownloader.net/
{...}

lgbridge 1.2.56 [Approved] Downloads cached for licensed users - Possibly broken for FOSS users (due to original download location changes by vendor)
 Title: LG Bridge | Published: 28/10/2019
 {...}
 Chocolatey Package Source: https://github.com/valentind44/chocolatey-packages/tree/master/automatic/lgbridge
 Package Checksum: '0pgT8hkgb398Z8APTec2fjELer6Di6nRj5An1lFIma5jiTRaiepP8CWYjZQ1Nhy2DgIQgxJppfV4Y+j0FiZJVg==' (SHA512)
 Software Site: https://www.lg.com/us/support/product-help/CT10000025-1438110404543-lg-backup-lg-switch
{...}

dacia-media-nav-toolbox 3.18.5.740218 [Approved] Downloads cached for licensed users
 Title: Dacia Media Nav Toolbox | Published: 10/08/2018
{...}
 Chocolatey Package Source: https://github.com/valentind44/chocolatey-packages/tree/master/automatic/dacia-media-nav-toolbox
 Package Checksum: 'jyrGsOfXayOKfEzchoYL24gStuPylh3nL0vyFYBWQQLAf2NKH2nVfHO29Y3v2svKrffrZN//wqlF43rggQ7pbA==' (SHA512)
 Software Site: https://dacia.naviextras.com/shop/portal/downloads
 {...}

vswhere 2.8.4 [Approved]
 Title: Visual Studio Locator | Published: 28/10/2019
 {...}
 Chocolatey Package Source: https://github.com/Microsoft/vswhere/tree/ff0de50053eff64ef583d019e516edc085185773/pkg/vswhere
 Package Checksum: 'ApZGGVhG502d/OSg0Eud1uOJ11QLbTJ78AQM17O+b0GQ0m5B9Z/BW/chf/7vwCSqTV6MFicKiyJeXzpEQ0ClkQ==' (SHA512)
 Software Site: https://github.com/Microsoft/vswhere
 Software Source: https://github.com/Microsoft/vswhere/tree/ff0de50053eff64ef583d019e516edc085185773
{...}

javaruntime 8.0.231 [Approved]
 Title: Java Runtime (JRE) | Published: 29/10/2019
{...}
 Chocolatey Package Source: https://github.com/proudcanadianeh/ChocoPackages/tree/master/javaruntime
 Package Checksum: 'Ps+FSS1/wtLEz/fP6Zchd9SCiTnU4JiqQmbEVWys/FQVeH3Ql4HZY9+5SLEAmelWTGyf9xcCTHNXslLQoSJkoA==' (SHA512)
 Software Site: http://www.java.com/
{...}

windowsphonedevelopertools 1.0 [Approved]
 Title: WindowsPhoneDeveloperTools | Published: 23/08/2011
{...}
 Chocolatey Package Source: n/a
 Package Checksum: 'JLrHs0UgciwXPbizd5UGxiUwU5WeiLY+DO3N8Rlc++QCulRFvjRH/+gkCN/Liz1DoBxIeTP3tVA+qTsNGeQKgg==' (SHA512)
 Software Site: http://msdn.microsoft.com/en-us/library/ff402523(v=vs.92).aspx
 
PowerShell 5.1.14409.20180811 [Approved]
 Title: Windows Management Framework and PowerShell | Published: 11/08/2018
 {...}
 Chocolatey Package Source: n/a
 Package Checksum: 'TLkGQn2o+PSZCFMdej48pnorwg0ZU4TXaMVRHopxQ3nXTl1bVyOJWsDPa/m4HPTOXtyCgJe525MDF8TWGdpTiQ==' (SHA512)
 Software Site: http://blogs.msdn.com/b/powershell/archive/2015/12/16/windows-management-framework-wmf-5-0-rtm-is-now-available.aspx
 {...}

vidcutter 6.0.0.1 [Approved] Downloads cached for licensed users
 Title: VidCutter | Published: 12/08/2018
{...}
 Chocolatey Package Source: https://github.com/ozmartian/vidcutter/blob/master/_packaging/chocolatey
 Package Checksum: '7EsSuh5GC11l1PWPQXorfh83hIzN2CnKmQvYRrZHEsj2/0L2DV5f/lnYPkR8TGQ/k0INkP34xP7d8zGCMn0Lmg==' (SHA512)
 Software Site: https://vidcutter.ozmartians.com/
 Software Source: https://github.com/ozmartian/vidcutter.git
 {...}

lastpass-for-applications 4.35.0 [Approved] Downloads cached for licensed users
 Title: LastPass for Applications | Published: 29/10/2019
 {...}
 Chocolatey Package Source: https://github.com/bdukes/Chocolatey-Packages
 Package Checksum: '7b+2VF7tcVhez4j5zULCQAztoiUCRFRuWksya+grI5JjUFr10CWDn5QqU/7koi1CWujEQWybaEdmlYdNlQ7eig==' (SHA512)
 Software Site: https://helpdesk.lastpass.com/upgrading-to-premium/lastpass-for-applications/
{...}

dropkick.core 0.4.13.0 [Approved]
 Title: DropkicK Core | Published: 31/10/2011
 {...}
 Chocolatey Package Source: n/a
 Package Checksum: 'kvETY7oUveJwrq5EJqjvDiwM1lTpQxR9B2b1ZljvxWL5yGlV27K30sXwVqw5kAtDcjBZ4PdbyX5zdpBQUrmtmw==' (SHA512)
 Software Site: https://github.com/chucknorris/dropkick
 {...}

dropkick 0.4.13.0 [Approved]
 Title: DropkicK | Published: 31/10/2011
 {...}
 Chocolatey Package Source: n/a
 Package Checksum: 'cNT/I9se0UEPr9aZ+y3ejVTFjmy8rxhTFIroTGKJU+BbIDqMLkwofYzjCOtgUYJmiYyRjJkhniwmFw2Ejv1E6A==' (SHA512)
 Software Site: https://github.com/chucknorris/dropkick
{...}

clonespy 3.43 [Approved] Downloads cached for licensed users
 Title: CloneSpy | Published: 29/10/2019
{...}
 Chocolatey Package Source: https://github.com/Windos/chocolatey/tree/master/clonespy
 Package Checksum: 'q4j9JVdlqP8sUvSnv+YI/fWLN3sG48o/EwvU/lrM9vFjgu6jgq+mCdA8z5Qt+lN83pQIRtMVOfIDnz4z1S4BEA==' (SHA512)
 Software Site: http://www.clonespy.com/
 {...}

ipfs-mount 0.3.3 [Approved]
 Title: Mount IPFS | Published: 14/08/2018
{...}
 Chocolatey Package Source: https://github.com/richardschneider/net-ipfs-mount
 Package Checksum: 'rJmme4xq4O2nvnDcexVEvIjFply5Ag7pMgY30bbOF7AYeYGlb/K+Em43jhITtKje73k5edPCINuyb6342iESYw==' (SHA512)
 Software Site: https://github.com/richardschneider/net-ipfs-mount
{...}

netcat 1.12 [Approved]
 Title: Netcat | Published: 16/08/2018
 {...}
 Chocolatey Package Source: n/a
 Package Checksum: 'qM4siEkryVPW2ywCFxQ5XsY/0fiP/P+lcoudF/9pyhMGS9K1QIcPfRHbGRXwC3Xai+iSXAC0SaPIL3Jv2aKDxQ==' (SHA512)
 Software Site: http://netcat.sourceforge.net/
{...}

adobereader-update 18.011.20058 [Approved] Downloads cached for licensed users
 Title: Adobe Reader DC Update | Published: 16/08/2018
 Chocolatey Package Source: https://github.com/aronovgj/choco-auto
 Package Checksum: 'NBypninotRVBEu0UFIlBliJqunMZ9vPJLg0zFpQUleGP/hId64QiDP8GTnfnwxA0XErTgHrFLRhyCheI/WGfVA==' (SHA512)
 Software Site: http://www.adobe.com/products/reader.html
{...}

DeleGate 9.9.7 [Approved]
 Title: DeleGate | Published: 04/01/2012
 {...}
 Chocolatey Package Source: n/a
 Package Checksum: 'hbAvgV8VHPzqQVJzUkBkOphvTksFOCNQwe1ykb36mu1krrC7Niei8qLYv8d8K++7as6bJTaZiSMTlWt5Qg5sAg==' (SHA512)
 Software Site: http://delegate.org/delegate/
 {...}

DarkRoom 0.8.1
 Title: Dark Room | Published: 22/01/2012
 {...}
 Chocolatey Package Source: n/a
 Package Checksum: 'KgSP9mMaYwdvkjpPM5xoPrEnxewtepQvTze2smHLNuHWt6Ht/iODtlzLqk7eiMnKohtmVP/h/sL5CSLVy8Tc8Q==' (SHA512)
 Software Site: https://github.com/jjafuller/DarkRoomW
{...}

msxml4.sp3 4.30.2100 [Approved] Downloads cached for licensed users
 Title: MSXML 4.0 Service Pack 3 (Microsoft XML Core Services) | Published: 15/08/2018
 {...}
 Chocolatey Package Source: n/a
 Package Checksum: 'cA7zwDLNAa26V4QCUzau+gXLRA86SCn1JRlnGTUDTVhtfM3oljJs8iUaZBcyX+kWqKyeOc0+ed3jd/qoYvTOJA==' (SHA512)
 Software Site: https://www.microsoft.com/en-us/download/details.aspx?id=15697
{...}

GnuWin 0.6.3.1 - Possibly broken
 Title: GnuWin | Published: 22/01/2012
 {...}
 Chocolatey Package Source: n/a
 Package Checksum: 'WbGG+W4vZ901wwsq0tWqxA6AAGny1b6LHTOQ/vfa+YcfSTzmTZUwvSLaKEYhGmjGs3dYt10fALpwO5VT2ZMCNw==' (SHA512)
 Software Site: http://gnuwin32.sourceforge.net/
{...}

ctags 5.8.1
 Title: Exuberant Ctags | Published: 22/01/2012
 {...}
 Chocolatey Package Source: n/a
 Package Checksum: 'FCOY3CChjGRNsfJvm6A1WxwlRu54zc9dIws3wAzYyuWJk+0sbaVkynz2VKAcKDX3vTPDyBNc6MDu8Lqen3o+Nw==' (SHA512)
 Software Site: http://ctags.sourceforge.net/
 {...}

PomoTime 1.8
 Title: PomoTime | Published: 24/01/2012
{...}
 Chocolatey Package Source: n/a
 Package Checksum: 'LRr9yXY2hlD8I0FI6rfJcuV0THJSLwckUmes0arXkBYhMjTViH3SAkUCG/f9u8zBLIbqZBhvB8sWFBSbjWs/Qg==' (SHA512)
 Software Site: http://www.xoring.com/
{...}

go-repo-utils 0.0.17 [Approved]
 Title: go-repo-utils | Published: 15/08/2016
 {...}
 Chocolatey Package Source: n/a
 Package Checksum: 'CXhqxH9VRrT4oFvjDpcFlHqiQwpWODdalrm8IsBwTMxT438MuUxPEQfSf0hr07yp24jR1ORM5YM2oJTy+ejWzA==' (SHA512)
 {...}
xnews 5.4.25 - Possibly broken
 Title: Xnews | Published: 29/01/2012
{...}
 Chocolatey Package Source: n/a
 Package Checksum: 'O1QWE3hqqQwEIKjeQFLlX5rQkQRnqL5InLfLjV58Qj8XEg6PpO3lhvlkA1qZow5qRCXqL5t4WdAp+uPdpvvZ9Q==' (SHA512)
{...}

onedrivebully 1.2 [Approved]
 {...}
 Chocolatey Package Source: https://github.com/teknowledgist/Chocolatey-packages/tree/master/automatic/onedrivebully
 Package Checksum: 'rXArDh/G5yK49nddpsTzWBuCW4nwWHeIyprYYGRf49ChEDtpX6x5IJULuTKiXwgKf/4gliPAPQG60aPoOIyryw==' (SHA512)
 {...}
 Software Source: https://github.com/ktheod/OneDriveBully
 {...}
 
cmdaliases 1.0.0.1
 Title: CmdAliases | Published: 06/02/2012
 {...}
 Chocolatey Package Source: n/a
 Package Checksum: 'IVVwN8oe+6w0oq6f8hfuS//rb4V3+hImZWdCSVHabbwnRUd/YdET+qD6rkp3I6fY6ls8ZcnC11r2QgjTo7mW5Q==' (SHA512)
 Software Site: http://ben.versionzero.org/wiki/Doskey_Macros
 {...}

backupper-standard 5.3.0 [Approved] Downloads cached for licensed users
 Title: AOMEI Backupper Standard (Install) | Published: 24/10/2019
 {...}
 Chocolatey Package Source: https://github.com/bcurran3/ChocolateyPackages/tree/master/backupper-standard
 Package Checksum: 'eAoW0JsNdXjfajXmp+ZwDHALcoQSO8uyLjiqBK+CE8JmkR4OJUQQLrVjxistObaKdypfR1LvXeT/j6xIYaPT+Q==' (SHA512)
{...}

backupper-server 5.3.0 [Approved] - Possibly broken
 Title: AOMEI Backupper Server (Install) | Published: 24/10/2019
 {...}
 Chocolatey Package Source: https://github.com/bcurran3/ChocolateyPackages/tree/master/backupper-server
 Package Checksum: 'r+GOpOc3EuRgkEyNWjp8l/GNR95/EJIINuT9lGsl+922dUOydngxS59WOJfJy6RP7mxTMY8pxcXUb+lN7dZs0Q==' (SHA512)
 {...}

sqlnotebook 0.6.0 [Approved] Downloads cached for licensed users
 Title: SQL Notebook | Published: 24/08/2016
 {...}
 Chocolatey Package Source: https://github.com/electroly/sqlnotebook/tree/master/src/chocolatey
 Package Checksum: 'u4A/RO35EnP47zUlWAmZgIiCPKZrb0Z8zAZQUw2waeAtfrlcMohwsBLHdEK1DSk2ePAbmhtXq/PVV9CnFMOZvQ==' (SHA512)
 {...}
 Software Source: https://github.com/electroly/sqlnotebook
 {...}

initool 0.9.0 [Approved] Downloads cached for licensed users
 Title: initool | Published: 27/07/2018
 {...}
 Chocolatey Package Source: https://github.com/dtgm/chocolatey-packages/tree/master/automatic/initool/
 Package Checksum: '3aJvt+p5uw536Hb120oUmChm0QjH69iZvlk/y73RjvUvu6gXVe7eYk/X/QvkP3jXokOMYe3DgUDyou2s2wPf0g==' (SHA512)
{...}
 Software Source: https://github.com/dbohdan/initool
 {...}

zip.template 1.0.0 [Approved]
 Title: Chocolatey Zip Template | Published: 18/08/2016
 {...}
 Chocolatey Package Source: https://github.com/ferventcoder/chocolatey-packages
 Package Checksum: '/U6Xwkq+YKE51QB98hTrInNgVClDdKnMfC4baYPIKDQiDXtqZgRKcigKAQupUJhzsIXsL5f1yB7+2du/Knq9PQ==' (SHA512)
 {...}
 Software Source: https://github.com/chocolatey/choco
 {...}
 
marker 0.0.6.0 [Approved]
 Title: Marker | Published: 25/02/2012
 {...}
 Chocolatey Package Source: n/a
 Package Checksum: 'aVDpEONsp/zZ/D6DZVCF+C/zbk64yjNFZ074PHwXUMFqRpMUaCRo7/tsJ91JPnTyIQ0Bw+RXnfjNZDZOPycwcg==' (SHA512)
{...}
 Software Site: https://github.com/chrisledet/Marker
 {...}
 
wcat 6.4.0 [Approved]
 Title: Web Capacity Analysis Tool (WCAT) | Published: 12/04/2012
 {...}
 Chocolatey Package Source: n/a
 Package Checksum: '2BSW0cur/XIcchXnQCjUEc/9Y9LYSTBG7dJP729wSQzJshN08cKZhR2YpaWlgdElgi/caoNu1iV3OwBsJgb0yw==' (SHA512)
 Software Site: http://www.iis.net/community/default.aspx?tabid=34&g=6&i=1466
 {...}

eazfuscator.net 3.3 [Approved]
 Title: Eazfuscator.NET | Published: 11/04/2012
 {...}
 Chocolatey Package Source: n/a
 Package Checksum: 'Ut4BZamN+Myv4UgTaBkTzdeTT/jmz6hOlNUNnQ0nmPoNTdMGAsXshWnxYyJl8zoaHuji7tlf0dkvfA9K1Afxgw==' (SHA512)
 Software Site: http://www.foss.kharkov.ua/g1/projects/eazfuscator/dotnet/Default.aspx
 {...}

tfsSidekicks2010 3.1.1
 Title: Team Foundation Sidekicks 2010 | Published: 28/02/2012
 {...}
 Chocolatey Package Source: n/a
 Package Checksum: 'T02Q7EP7UTkzvnuYF40zpe3DNWaV6uRu85DRpai/cH2fCnZTvdcuyUMjsCqyCmFny/Kb1QFS3LQzSOByTIJCwQ==' (SHA512)
 Software Site: http://www.attrice.info/cm/tfs/
 {...}

WinVDIG 1.0.5
 Title: WinVDIG | Published: 11/05/2012
 {...}
 Chocolatey Package Source: n/a
 Package Checksum: 'TZ/s6w7I236HHZ1Uqb34FlIniVMar1MS9NZ0ZvonUiAP95wXOCzrDdwmbwcjvXMnRYVj17UEfogh5BmNmsqTag==' (SHA512)
 Software Site: http://www.eden.net.nz/7/20071008/
 {...}
 
dotnet-verification-tool 4.6.01528.0 [Approved] Downloads cached for licensed users
 Title: .NET Framework Setup Verification Tool (Portable) | Published: 31/08/2016
 {...}
 Chocolatey Package Source: n/a
 Package Checksum: 'gxIBC3A6bPqI19Ouw37alJOx9pJG2aVNHybAHCxr+OGfpXajjO/1b3gxnVfvpo27waXvMM/FI2GRa9EHZVaTSw==' (SHA512)
 Software Site: https://blogs.msdn.microsoft.com/astebner/2008/10/13/net-framework-setup-verification-tool-users-guide/
 {...}
 
CoffeeScript 1.3.1.01
 Title: CoffeeScript | Published: 14/05/2012
 {...}
 Chocolatey Package Source: n/a
 Package Checksum: 'xM8Xg6F9TTMSTbFWWi2mpQoHQVnx8IykQbQpf8A30TEqFZwXXWoWquzOHh3cSUwu3kNGMqCYjq0XIFeuhbuIGw==' (SHA512)
 Software Site: http://coffeescript.org/
{...}

real-netstat 3.1 [Approved] Downloads cached for licensed users
 Title: Real NetStat (Trial) (Install) | Published: 22/08/2016
 {...}
 Chocolatey Package Source: n/a
 Package Checksum: 'iwKRlOh3GqFO2kSFDmgaGhS37cU/8AnM8PRoB0aIiJ+VNfgZBpYfxxwGHy+H/XRdlo5BGlNvgPA+D0U+qIqTWw==' (SHA512)
 Software Site: http://netstatagent.com/real-netstat/
 {...}

clojure.clr 1.3.0
 Title: Clojure.clr | Published: 17/05/2012
 {...}
 Chocolatey Package Source: n/a
 Package Checksum: 'g7tSWU6FkMD4MtdYh+BpDATtJGUAl564fV/CGUON1Gc1lY2bjYudTYlBccPO8deY8/hrvy1VkzVCGwoDwtBGzA==' (SHA512)
 Software Site: https://github.com/clojure/clojure-clr/wiki
 {...}
 
xboot 1.0.14 [Approved] Downloads cached for licensed users
 Title: XBoot (Portable) | Published: 24/08/2016
 {...}
 Chocolatey Package Source: n/a
 Package Checksum: 'T7mEVVL3Gz1tT2mW+aTyCQAF/lO6Hs+oYdZRjph8fftivheY2uxvgzsBOIZHCBiZSCu7uq72qoC2FhLUo71EPA==' (SHA512)
 Software Site: https://sites.google.com/site/shamurxboot/home
 {...}

winhotkey 0.70.0.1 [Approved] Downloads cached for licensed users
 Title: WinHotKey | Published: 22/08/2016
 {...}
 Chocolatey Package Source: n/a
 Package Checksum: '7HDbMGIPziOGZKbbnu+lcx2k/QjqgKuE9WLrmaIbnYGDmPpxdRbwYtBGqMf+c/JAa5FJtA7M6GNUlA33wEQo/g==' (SHA512)
 Software Site: http://directedge.us/content/winhotkey
 {...}

mobipocket.reader 6.2
 Title: Mobipocket Reader Desktop | Published: 21/05/2012
 {...}
 Chocolatey Package Source: n/a
 Package Checksum: 'oKpSJB7XTjTZkcSyKOp59pzMkXJIBqSQ76DnVoYoubGr4xhrmImdmpYgNu5wO+xRnXWZC0gYYoeK6nZYD018/g==' (SHA512)
 Software Site: http://www.mobipocket.com/en/downloadsoft/productdetailsreader.asp
 {...}

evince 2.32.0.145001 [Approved] Downloads cached for licensed users
 Title: Evince | Published: 22/08/2016
 {...}
 Chocolatey Package Source: n/a
 Package Checksum: 'M+MjhcuFqE4KK32QwAJm6XH1+YdQhGEFmVX89gvGNdmeAr5DfVb1XnnKbcYxQREuMjwFEA+AomkE3i9/bX1BFw==' (SHA512)
 Software Site: https://wiki.gnome.org/Apps/Evince
{...}

viper4windows 1.0.5.1 [Approved] Downloads cached for licensed users
 Title: ViPER4Windows | Published: 22/08/2016
 {...}
 Chocolatey Package Source: n/a
 Package Checksum: 'Z+5mCMLRRxKvk1XCL/XOM2QgfPiNfWTSf7pF+Z05dtm7RCSh0WHCgIlyywDBnmly1WIpKHWBRioEQHerGhP1qw==' (SHA512)
 Software Site: http://vipersaudio.com/blog/?page_id=59
 {...}

factor 0.97 [Approved] Downloads cached for licensed users
 Title: Factor | Published: 25/08/2016
 {...}
 Chocolatey Package Source: http://github.com/clementi/chocolatey-factor
 Package Checksum: 'ZRfcoOeRIp7WJ8FgFG+q8fSbwAsGvNEqRBMKfjn/VX4lDdnsy/m0Axh47UEaLRXFXWuOjkZQt83WWQPdk5cFRg==' (SHA512)
 Software Site: http://factorcode.org/
 {...}
59 packages found.
```